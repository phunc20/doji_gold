import datetime

import numpy as np
import pandas as pd
import scrapy


class GoldSpider(scrapy.Spider):
    name = "gold"
    start_urls = ["https://doji.vn/bang-gia-vang/"]

    def parse0(self, response):
        for table in response.xpath('//div[@class="_table"]'):
            title = table.xpath('*/div[@class="_taxonomy"]/div[@class="_title-tax"]/text()').get().strip()
            keyword = "tại"
            if keyword not in title:
                continue
            else:
                city = "".join(s[0].lower() for s in title.split(keyword)[1].split())

    def parse(self, response):
        city2df = {}
        for table in response.xpath('//div[@class="_table"]')[1:]:
            title = table.xpath('*/div[@class="_taxonomy"]/div[@class="_title-tax"]/text()').get().strip()
            city = "".join(s[0].lower() for s in title.split("tại")[1].split())

            index = []
            for s in table.xpath(
                '*/div[@class="_taxonomy"]/div[@class="_block"]/text()'
            ).getall():
                stripped = s.strip()
                if stripped != "":
                    index.append(stripped)

            buy = []
            for s in table.xpath(
                '*/div[@class="_buy"]/div[@class="_block"]/text()'
            ).getall():
                stripped = s.strip()
                if stripped == "-":
                    #price = np.nan
                    price = None
                else:
                    price = int(stripped.replace(",", ""))
                buy.append(price)

            sell = []
            for s in table.xpath(
                '*/div[@class="_Sell"]/div[@class="_block"]/text()'
            ).getall():
                stripped = s.strip()
                if stripped == "-":
                    #price = np.nan
                    price = None
                else:
                    price = int(stripped.replace(",", ""))

                sell.append(price)

            #columns = ["buy", "sell"]
            data = {
                "buy": buy,
                "sell": sell,
            }
            # https://pandas.pydata.org/docs/user_guide/integer_na.html
            df = pd.DataFrame(
                data=data,
                index=index,
                #columns=columns,
                dtype="Int64",
            )
            city2df[city] = df

        timestamp = table.xpath('p[@class="_desc"]/text()').get().rstrip()[-16:]
        # Doji badly represents 13h as 01 w/o am/pm specification
        vn_timezone = datetime.timezone(datetime.timedelta(hours=7))
        vn_now = datetime.datetime.now(vn_timezone)
        if int(timestamp[:2]) + 12 == vn_now.hour:
            timestamp = f'{vn_now.hour}{timestamp[:2]}'

